extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var label
# Called when the node enters the scene tree for the first time.
func _ready():
	label = get_node("Label")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	label.text = "Score : " +String( GameManager.Score)
