extends Node2D

export (Vector2) var Direction
export (float) var Speed

export (float) var speedIncreasePerMin
var Area
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	Area = get_node("Area2D")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var objs =Area.get_overlapping_areas()
	
	for i in objs:
		
		var item = i.get_parent()
		if item is preload("Scripts//HotDogBun.gd"):
			item.position += Direction * Speed * delta
		
	Speed +=( delta * speedIncreasePerMin) / 60
	
