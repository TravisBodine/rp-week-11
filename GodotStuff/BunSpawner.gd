extends Node2D

export(String) var itemPath

var area 
var colShape
# Called when the node enters the scene tree for the first time.
func _ready():
	area = get_node("Area2D")
	colShape = area.get_node("CollisionShape2D")
	pass # Replace with function body.

func spawnBun():
	randomize()
	var scene = load("res://" +itemPath+ ".tscn")
	if scene != null:
		var centerpos = colShape.position + area.position
		var size = colShape.shape.extents
		
		var positionInArea = Vector2()
		
		positionInArea.x = (randi() %int(round (size.x))) - (size.x/2) + centerpos.x
		positionInArea.y = (randi() % int(round(size.y))) - (size.y/2) + centerpos.y
		
		var node = scene.instance()
		add_child(node)
		node.position = positionInArea
	
