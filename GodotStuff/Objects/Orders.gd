extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(int)var maxOrders
export(int) var maxCondiments
export(float) var SpawnSpeed
export(float )var SpawnSpeedDecreasePerMinute

var PossibleCondiments = Array()
var bunSpawner 
var timer = 0
#var Orders = Array()
var OrderVisual
# Called when the node enters the scene tree for the first time.
func _ready():
	bunSpawner = get_node("BunSpawner")
	PossibleCondiments.append(load("res://Objects/HotDog.tscn").instance())
	OrderVisual = load("res://Objects/OrderVisual.tscn")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(GameManager.Orders.size()<maxOrders):
		timer -= delta
		if(timer <= 0):
			makeOrder()
			if SpawnSpeed > 1:
				SpawnSpeed -=( delta * SpawnSpeedDecreasePerMinute) / 60
			timer = SpawnSpeed
	
func makeOrder():
	var randAmountOfCondiments = int(round(rand_range(1,maxCondiments)))
	#print (randAmountOfCondiments)
	
	var newOrder = order.new()
	for i in randAmountOfCondiments:
		var randCondindex = int(round(rand_range(0,PossibleCondiments.size()-1)))
		newOrder.Condiments.append( PossibleCondiments[randCondindex])
		
	GameManager.Orders.append(newOrder)
	
	var visual= OrderVisual.instance()
	GameManager.OrderVisuals.append(visual)
	visual.get_node("Label").text = newOrder.toString()
	get_node("GridContainer").add_child(visual)
	
	
	bunSpawner.spawnBun()
	

class order : 
	
	var Condiments = Array()
	func setArray(array):
		Condiments = array
		#print(Condiments)
	func toString() -> String:
		var string = ""
		for c in Condiments:
			string +=(c.name)+"\n"
		return string