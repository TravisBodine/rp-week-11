extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var interactable = true
var PickedUp = true
var area
var offset = Vector2()
var targ = Node2D
# Called when the node enters the scene tree for the first time.
func _ready():
	
	area = get_node("Area2D")
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("click"):
		if(interactable):
			var catHand = get_tree().root.get_node("BaseGame").get_node("CatHand")
			if catHand != null:
				if area.overlaps_area(catHand.get_node("Area2D")):
					#catHand.isHoldingAnItem = true
					PickedUp = true
					catHand.item = self
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print(targ)
	if targ != null:
		position = targ.get_global_position() + offset
	
	#	print(targ.name)
	#else:
		#print(targ)
