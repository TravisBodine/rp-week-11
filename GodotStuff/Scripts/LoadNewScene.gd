extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var button_path
#onready var button = get_node(button_path)
export (String) var scene_to_load
export (String) var sceneName


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Button_pressed():
		#check if the scene exists
	var next_level_path = ResourceLoader.load("res://"+scene_to_load + ".tscn")
	if(next_level_path != null):
		# Remove the current level
		var BaseGame = get_tree().get_root().get_node("BaseGame")
		var currMenu = BaseGame.get_node(sceneName)
	
		print(BaseGame.get_node(sceneName))
		
		BaseGame.remove_child(currMenu)
		currMenu.call_deferred("free")

		# Add the next level
		var next_level = next_level_path.instance()
		BaseGame.add_child(next_level)
