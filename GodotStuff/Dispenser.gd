extends Node2D

export (String) var itemPath

export (PackedScene) var CatHand

var area

# Called when the node enters the scene tree for the first time.
func _ready():
	area = get_node("Area2D")
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("click"):
		var catHand = get_tree().root.get_node("BaseGame").get_node("CatHand")
		if catHand != null:
			if area.overlaps_area(catHand.get_node("Area2D")):
				var scene = load("res://" +itemPath+ ".tscn")
				if scene != null:
					var node = scene.instance()
					
					catHand.isHoldingAnItem 
					catHand.item = node
					#node.call(_ready())
					
					get_tree().get_root().add_child(node)
		
	if event is InputEventMouseButton and !event.pressed:
		var droppedItem = area.get_overlapping_areas()
		if droppedItem != null:
			for i in droppedItem:
				var obj = i.get_parent()
				if obj != get_tree().root.get_node("BaseGame").get_node("CatHand"):
					print(obj.name)
					obj.get_parent().remove_child(obj)
					obj.call_deferred("free")
					
